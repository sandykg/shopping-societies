package com.shoppingsocieties.constants;

public final class ErrorConstants {

    // Spring profile for development, production and "fast", see http://jhipster.github.io/profiles.html
    public static final String RESOURCE_NOT_FOUND = "404 - Resource Unavailable";

    private ErrorConstants() {
    }
}