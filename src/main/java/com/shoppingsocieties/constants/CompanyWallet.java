package com.shoppingsocieties.constants;

/**
 * Company Wallet ID kept as a static access
 */
public class CompanyWallet {
    public static final Long ID = 1L;

    private CompanyWallet() {
    }
}
