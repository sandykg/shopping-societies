package com.shoppingsocieties.constants;

public final class MessageConstants {

    public static final String RESOURCE_NOT_FOUND = "Resource Not Found";
    public static final String RESOURCE_LIST_NOT_FOUND = "Resource List Not Found";
    public static final String INVALID_PARAMETER = "Invalid Input Parameter(s) Type";
    public static final String WALLET_NOT_FOUND = "Wallet for the given User Not Found" ;
    public static final String COMPANY_WALLET_NOT_FOUND = "Company Wallet Not Found";
    public static final String SENDER_WALLET_NOT_FOUND = "Sender's Wallet Not Found" ;
    public static final String INSUFFICIENT_BALANCE = "Sender's Wallet Balance insufficient";
    public static final String PURCHASE_SUCCESSFUL = "Purchase and Fund Transfer Successful";
    public static final String PRODUCT_NOT_FOUND = "Product for the given ID Not Found";
    public static final String PRODUCT_SOLD_OUT = "Product Sold Out";
    public static final String SAME_WALLET_ERROR = "You cannot send money to yourself";

    private MessageConstants() {
    }
}