package com.shoppingsocieties.rest;

import com.shoppingsocieties.domain.Product;
import com.shoppingsocieties.domain.User;
import com.shoppingsocieties.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller for managing Product.
 */

@RestController
@RequestMapping("/api/v1")
public class ProductResource {
    private final Logger log = LoggerFactory.getLogger(ProductResource.class);

    @Autowired
    private ProductService productService;

    /**
     * POST  /products/{id}/purchase : Product purchase API and transfer from user to company's wallet.
     *
     * @param id the id of product to be purchased by User
     * @return a Response Entity String with status 200, status 400 (Bad Request) or status 500 (Internal Server Error)
     */
    @PostMapping("/products/{id}/purchase")
    public ResponseEntity<Product> purchaseProduct(@PathVariable Long id, @RequestBody User user) {
        log.debug("REST request to purchase Product {}" + id + " by User {}" + user.getId());
        Product purchasedProduct = productService.productPurchase(id, user.getId());
        return new ResponseEntity<>(purchasedProduct, HttpStatus.OK);
    }
}
