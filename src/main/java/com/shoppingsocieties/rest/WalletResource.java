package com.shoppingsocieties.rest;

import com.shoppingsocieties.domain.Wallet;
import com.shoppingsocieties.service.WalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing the Wallet Account of User.
 */

@RestController
@RequestMapping("/api/v1")
public class WalletResource {
    private final Logger log = LoggerFactory.getLogger(WalletResource.class);

    @Autowired
    private WalletService walletService;

    /**
     * GET  /wallet/:id : get the wallet resource by giving "id".
     *
     * @param id the id of the wallet resource to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sales list or with status 400 (Bad Request) or
     * 404 (Not Found)
     */
    @GetMapping("/wallet/{id}")
    public ResponseEntity<Wallet> getWallet(@PathVariable Long id) {
        log.debug("REST request to get wallet : {}", id);
        Wallet wallet = walletService.findOneById(id);
        return new ResponseEntity<>(wallet, HttpStatus.OK);
    }
}
