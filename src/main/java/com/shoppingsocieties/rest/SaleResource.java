package com.shoppingsocieties.rest;

import com.shoppingsocieties.domain.Sale;
import com.shoppingsocieties.service.SaleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller for managing Flash Sale.
 */

@RestController
@RequestMapping("/api/v1")
public class SaleResource {

    private final Logger log = LoggerFactory.getLogger(SaleResource.class);

    @Autowired
    private SaleService saleService;

    /**
     * GET  /sales/current : get the wallet resource by giving a request parameter "country".
     *
     * @param country where the flash sale is requested for
     * @return the ResponseEntity with status 200 (OK) and with body the sales list or with status 400 (Bad Request) or
     * 404 (Not Found)
     */
    @GetMapping("/sales/current")
    public ResponseEntity<List<Sale>> getCurrentSale(@RequestParam("country") String country) {

        log.debug("REST request to get current sales of : {}" + country);
        List<Sale> saleList = saleService.findAllByCountry(country);
        return new ResponseEntity<>(saleList, HttpStatus.OK);
    }
}