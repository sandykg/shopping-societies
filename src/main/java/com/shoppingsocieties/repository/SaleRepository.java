package com.shoppingsocieties.repository;


import com.shoppingsocieties.domain.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Sale entity.
 */

@Repository
public interface SaleRepository extends JpaRepository<Sale, Long> {
    List<Sale> findAllByCountry(String country);

    Optional<Sale> findOneByProductId(Long id);
}
