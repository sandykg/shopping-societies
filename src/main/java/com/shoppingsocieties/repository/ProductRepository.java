package com.shoppingsocieties.repository;

import com.shoppingsocieties.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the Product entity.
 */

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> findOneById(Long id);
}
