package com.shoppingsocieties.repository;

import com.shoppingsocieties.domain.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the Wallet entity.
 */
@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {

    Optional<Wallet> findOneById(Long id);

    Optional<Wallet> findByUserId(Long userId);
}
