package com.shoppingsocieties.service;

import com.shoppingsocieties.constants.MessageConstants;
import com.shoppingsocieties.domain.Wallet;
import com.shoppingsocieties.error.BadRequestException;
import com.shoppingsocieties.error.NotFoundException;
import com.shoppingsocieties.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * A service class for Wallet functionality
 */

@Service
public class WalletService {

    @Autowired
    private WalletRepository walletRepository;

    /**
     * Find one wallet object
     *
     * @param id Long object of the object to be found
     * @return Wallet object
     * @throws NotFoundException if the wallet for given id is not found
     */
    public Wallet findOneById(Long id) {
        Wallet wallet = walletRepository.findOne(id);
        if (Optional.ofNullable(wallet).isPresent()) {
            return wallet;
        } else {
            throw new NotFoundException(MessageConstants.WALLET_NOT_FOUND);
        }

    }

    /**
     * Validate the wallet balance of the sender, if success, facilitates the wallet transfer.
     * Synchronized block declared to prevent concurrency issues in receiving funds to Company Wallet
     *
     * @param sender         Wallet object of the sender/payer
     * @param transferAmount Double object of the amount to be transfereed
     * @param receiver       Wallet object of the receiver/payee
     * @return String object of message description
     */

    public String walletTransfer(Wallet sender, Double transferAmount, Wallet receiver) {
        if (sender.getBalance() <= 0 || sender.getBalance() < transferAmount) {
            throw new BadRequestException(MessageConstants.INSUFFICIENT_BALANCE);
        } else if (sender.equals(receiver)) {
            throw new BadRequestException(MessageConstants.SAME_WALLET_ERROR);

        } else {

            sender.setBalance(sender.getBalance() - transferAmount);
            receiver.setBalance(receiver.getBalance() + transferAmount);

            walletRepository.save(sender);
            walletRepository.save(receiver);

            return MessageConstants.PURCHASE_SUCCESSFUL;

        }
    }
}
