package com.shoppingsocieties.service;

import com.shoppingsocieties.constants.MessageConstants;
import com.shoppingsocieties.domain.Product;
import com.shoppingsocieties.domain.Sale;
import com.shoppingsocieties.error.BadRequestException;
import com.shoppingsocieties.error.NotFoundException;
import com.shoppingsocieties.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * A Service for Sale Functionality
 */

@Service
public class SaleService {

    @Autowired
    private SaleRepository saleRepository;


    /**
     * Find all Sale items for a specific country
     *
     * @param country code of the country with a two letter input
     * @return List<Sale> for the input country code
     * @throws NotFoundException if the specific country sale list is not found
     */
    public List<Sale> findAllByCountry(String country) {

        if (!country.matches("[a-zA-Z]{2}")) {
            throw new BadRequestException(MessageConstants.INVALID_PARAMETER);
        }
        String countryCode = country.toUpperCase(); // assumption: country code will be 2 caps character in length

        List<Sale> saleList = saleRepository.findAllByCountry(countryCode);

        if (!saleList.isEmpty()) {
            return saleList;
        } else {
            throw new NotFoundException(MessageConstants.RESOURCE_LIST_NOT_FOUND);
        }
    }

    /**
     * This method updates the current flash sale product
     *
     * @param product product entity of the flash sale purchase requested by user
     */
    public void updateProductSale(Product product) {
        Optional<Sale> sale = saleRepository.findOneByProductId(product.getId());
        Sale currentSale = sale.get();
        Integer currentItemsLeft = currentSale.getItemsLeft();
        currentSale.setItemsLeft(--currentItemsLeft);
        saleRepository.save(currentSale);
    }
}

