package com.shoppingsocieties.service;

import com.shoppingsocieties.constants.CompanyWallet;
import com.shoppingsocieties.constants.MessageConstants;
import com.shoppingsocieties.domain.Product;
import com.shoppingsocieties.domain.Sale;
import com.shoppingsocieties.domain.Wallet;
import com.shoppingsocieties.error.BadRequestException;
import com.shoppingsocieties.error.InternalServerException;
import com.shoppingsocieties.error.NotFoundException;
import com.shoppingsocieties.repository.ProductRepository;
import com.shoppingsocieties.repository.SaleRepository;
import com.shoppingsocieties.repository.WalletRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * A service class for Product functionality
 */
@Service
public class ProductService {


    private final Logger log = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private WalletService walletService;

    @Autowired
    private SaleService saleService;


    /**
     * Handle purchase of product and call wallet transfer method
     *
     * @param userId    id of the user who has request for flash sale purchase
     * @param productId id of the product requested for flash sale purchase by user
     * @return the product that has been purchased successfully
     * @throws InternalServerException,NotFoundException for different scenarios, we have custom exception description
     */

    public Product productPurchase(Long productId, Long userId) {

        Optional<Wallet> senderWallet = walletRepository.findByUserId(userId);
        Optional<Wallet> receiverWallet = walletRepository.findOneById(CompanyWallet.ID);
        Optional<Product> purchasedProduct = productRepository.findOneById(productId);

        if (!receiverWallet.isPresent()) {
            throw new InternalServerException(MessageConstants.COMPANY_WALLET_NOT_FOUND);

        } else if (!purchasedProduct.isPresent()) {
            throw new NotFoundException(MessageConstants.PRODUCT_NOT_FOUND);

        } else if (!senderWallet.isPresent()) {
            throw new NotFoundException(MessageConstants.SENDER_WALLET_NOT_FOUND);

        } else if (isProductSoldOut(purchasedProduct.get())) {
            throw new BadRequestException(MessageConstants.PRODUCT_SOLD_OUT);

        } else {
            walletService.walletTransfer(senderWallet.get(),
                    purchasedProduct.get().getPrice(), receiverWallet.get());
            saleService.updateProductSale(purchasedProduct.get());
            return purchasedProduct.get();
        }
    }

    /**
     * Check if the product is sold out returns boolean
     *
     * @param product product entity of the sale purchase requested by user
     * @return true or false
     */

    private boolean isProductSoldOut(Product product) {
        Optional<Sale> sale = saleRepository.findOneByProductId(product.getId());
        if (sale.isPresent()) {
            if (sale.get().getItemsLeft() <= 0)
                return true;
        }
        return false;
    }
}
