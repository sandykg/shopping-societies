-- Seed data into User Table
insert into user values(1, 'mark@fundingsocieties.com', 'Mark', 'Benson', 'password1234');
insert into user values(2, 'john.doe@somemail.com', 'John', 'Doe', 'password1234');
insert into user values(3, 'jane.low@somemail.com', 'Jane', 'Low', 'password1234');
insert into user values(4, 'ale1.poe@somemail.com', 'Alex', 'Poe', 'password1234');
insert into user values(5, 'ale2.poe@somemail.com', 'Alex', 'Poe', 'password1234');
insert into user values(6, 'ale3.poe@somemail.com', 'Alex', 'Poe', 'password1234');
insert into user values(7, 'ale4.poe@somemail.com', 'Alex', 'Poe', 'password1234');
insert into user values(8, 'ale5.poe@somemail.com', 'Alex', 'Poe', 'password1234');
insert into user values(9, 'ale6.poe@somemail.com', 'Alex', 'Poe', 'password1234');
insert into user values(10, 'al7x.poe@somemail.com', 'Alex', 'Poe', 'password1234');
insert into user values(11, 'al8x.poe@somemail.com', 'Alex', 'Poe', 'password1234');
insert into user values(12, 'al9x.poe@somemail.com', 'Alex', 'Poe', 'password1234');
insert into user values(13, 'al10x.poe@somemail.com', 'Alex', 'Poe', 'password1234');


-- Seed data into Wallet Table
insert into wallet values(1, 10000.00, 'SGD', 1);
insert into wallet values(2, 100.00, 'SGD', 2);
insert into wallet values(3, 120.00, 'SGD', 3);
insert into wallet values(4, 60.00, 'SGD', 4);
insert into wallet values(5, 60.00, 'SGD', 5);
insert into wallet values(6, 60.00, 'SGD', 6);
insert into wallet values(7, 60.00, 'SGD', 7);
insert into wallet values(8, 60.00, 'SGD', 8);
insert into wallet values(9, 60.00, 'SGD', 9);
insert into wallet values(10, 60.00, 'SGD', 10);
insert into wallet values(11, 60.00, 'SGD', 11);
insert into wallet values(12, 60.00, 'SGD', 12);

-- Seed data into Product Table
insert into product values(1, 'SKU001','Accessories','SGD', 100.00);
insert into product values(2, 'SKU002','Footwear', 'SGD',  35.50);
insert into product values(3, 'SKU003', 'Apparel', 'SGD', 33.00);
insert into product values(4, 'SKU004', 'Electronics','SGD',  50.00);
insert into product values(5, 'SKU005', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(6, 'SKU006', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(7, 'SKU007', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(8, 'SKU008', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(9, 'SKU009', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(10, 'SKU010', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(11, 'SKU01105', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(12, 'SKU0305', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(13, 'SKU03405', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(14, 'SKU00455', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(15, 'SKU03205', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(16, 'SKU00565', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(17, 'SKU0085', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(18, 'SKU05', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(19, 'SKU00235', 'Beauty & Personal Care','SGD', 50.00);
insert into product values(20, 'SKU00125', 'Beauty & Personal Care','SGD', 50.00);

-- Seed data into Sale Table
insert into sale values(1, 'SG', 300, 18000000, 900, 1);
insert into sale values(2, 'SG', 450, 10800000, 1000, 2);
insert into sale values(3, 'SG', 15, 10800000, 500, 3);
insert into sale values(4, 'TH', 360, 10800000, 600, 4);
insert into sale values(5, 'VN', 145, 10800000, 700, 5);
insert into sale values(6, 'VN', 145, 10800000, 700, 6);
insert into sale values(7, 'SG', 145, 10800000, 700,7);
insert into sale values(8, 'SG', 145, 10800000, 700, 8);
insert into sale values(9, 'SG', 145, 10800000, 700, 9);
insert into sale values(10, 'SG', 145, 10800000, 700, 10);
insert into sale values(11, 'SG', 145, 10800000, 700, 11);
insert into sale values(12, 'SG', 145, 10800000, 700, 12);
insert into sale values(13, 'SG', 145, 10800000, 700, 13);
insert into sale values(14, 'SG', 145, 10800000, 700, 14);
insert into sale values(15, 'SG', 145, 10800000, 700, 15);
insert into sale values(16, 'SG', 145, 10800000, 700, 16);
insert into sale values(17, 'SG', 145, 10800000, 700, 17);
insert into sale values(18, 'SG', 145, 10800000, 700, 18);
insert into sale values(19, 'SG', 145, 10800000, 700, 19);
insert into sale values(20, 'SG', 145, 10800000, 700, 20);
