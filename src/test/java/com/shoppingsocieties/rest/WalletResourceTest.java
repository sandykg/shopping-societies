package com.shoppingsocieties.rest;

import com.shoppingsocieties.domain.Wallet;
import com.shoppingsocieties.service.WalletService;
import com.shoppingsocieties.testmodelbuilder.TestWalletBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for Wallet REST Controller
 */
@RunWith(SpringRunner.class)
public class WalletResourceTest {

    @Autowired
    private WalletResource walletResource;

    @MockBean
    private WalletService walletService;

    @MockBean
    private ResponseEntity<?> testResponseEntity;

    @TestConfiguration
    static class WalletResourceTestConfig {
        @Bean
        public WalletResource walletResource() {
            return new WalletResource();
        }
    }

    @Test
    public void getCurrentSaleTest() {
        Long walletId = 1L;
        Wallet wallet = new TestWalletBuilder().id(1L).build();

        Mockito.when(walletService.findOneById(walletId)).thenReturn(wallet);
        testResponseEntity = new ResponseEntity<>(wallet, HttpStatus.OK);

        assertThat(walletResource.getWallet(walletId)).isEqualTo(testResponseEntity);
    }
}
