package com.shoppingsocieties.rest;

import com.shoppingsocieties.domain.Product;
import com.shoppingsocieties.domain.User;
import com.shoppingsocieties.service.ProductService;
import com.shoppingsocieties.testmodelbuilder.TestProductBuilder;
import com.shoppingsocieties.testmodelbuilder.TestUserBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for Product REST Controller
 */

@RunWith(SpringRunner.class)
public class ProductResourceTest {

    @Autowired
    private ProductResource productResource;

    @MockBean
    private ProductService productService;

    @MockBean
    private ResponseEntity<?> testResponseEntity;

    @TestConfiguration
    static class ProductResourceTestConfig {
        @Bean
        public ProductResource productResource() {
            return new ProductResource();
        }
    }

    @Test
    public void productPurchaseTest() {
        User testUser = new TestUserBuilder().id(1L).build();
        Long testProductId = 1L;
        Product purchasedProduct = new TestProductBuilder().id(1L).build();
        Mockito.when(productService.productPurchase(testProductId, testUser.getId())).thenReturn(purchasedProduct);
        testResponseEntity = new ResponseEntity<Object>(purchasedProduct, HttpStatus.OK);

        assertThat(productResource.purchaseProduct(1L, testUser)).isEqualTo(testResponseEntity);
    }
}
