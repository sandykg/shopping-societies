package com.shoppingsocieties.rest;

import com.shoppingsocieties.domain.Sale;
import com.shoppingsocieties.service.SaleService;
import com.shoppingsocieties.testmodelbuilder.TestSaleBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for Sale REST Controller
 */
@RunWith(SpringRunner.class)
public class SaleResourceTest {

    @Autowired
    private SaleResource saleResource;

    @MockBean
    private SaleService saleService;

    @MockBean
    private ResponseEntity<?> testResponseEntity;

    @TestConfiguration
    static class SaleResourceTestConfig {
        @Bean
        public SaleResource saleResource() {
            return new SaleResource();
        }
    }

    @Test
    public void getCurrentSaleTest() {
        String testCountry = "SG";
        Sale testSale = new TestSaleBuilder().id(1L).build();
        Sale testSale1 = new TestSaleBuilder().id(2L).build();
        List<Sale> testSaleList = new ArrayList<Sale>();
        testSaleList.add(testSale);
        testSaleList.add(testSale1);
        Mockito.when(saleService.findAllByCountry(testCountry)).thenReturn(testSaleList);
        testResponseEntity = new ResponseEntity<>(testSaleList, HttpStatus.OK);

        assertThat(saleResource.getCurrentSale(testCountry)).isEqualTo(testResponseEntity);
    }
}

