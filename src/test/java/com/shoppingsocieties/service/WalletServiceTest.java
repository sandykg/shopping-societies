package com.shoppingsocieties.service;

import com.shoppingsocieties.domain.Currency;
import com.shoppingsocieties.domain.User;
import com.shoppingsocieties.domain.Wallet;
import com.shoppingsocieties.repository.WalletRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Wallet Service Unit Test
 */

@RunWith(SpringRunner.class)
public class WalletServiceTest {

    @Autowired
    private WalletService walletService;

    @MockBean
    private WalletRepository walletRepository;

    @TestConfiguration
    static class WalletServiceTestConfig {
        @Bean
        public WalletService walletService() {
            return new WalletService();
        }
    }

    public Wallet createWallet(Long id, Double balance, Currency currency, User user) {
        Wallet wallet = new Wallet();
        wallet.setId(id);
        wallet.setBalance(balance);
        wallet.setCurrency(currency);
        wallet.setUser(user);
        return wallet;
    }

    public User createUser(Long id, String firstName, String lastName, String email) {
        User user = new User();
        user.setId(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        return user;
    }

    @Test
    public void walletFindOneByIdTest() {
        User user = createUser(1L, "John", "Doe", "john.doe@gmail.com");
        Wallet wallet = createWallet(100L, 1000.00, Currency.SGD, user);
        Mockito.when(walletRepository.findOne(100L)).thenReturn(wallet);

        Wallet found = walletService.findOneById(100L);
        assertThat(wallet.getUser().getFirstName()).isEqualTo(found.getUser().getFirstName());
    }

    @Test
    public void walletTransferTest() {
        User sender = createUser(1L, "Adam", "Doe", "adamdoe@gmail.com");
        User receiver = createUser(100L, "Shopping", "Societies", "shoppingsocieties@fs.com");
        Wallet receiverWallet = createWallet(100L, 100.00, Currency.SGD, receiver);
        Double transferAmount = 100.00;
        Wallet senderWallet = createWallet(101L, 100.00, Currency.SGD, sender);

        walletService.walletTransfer(senderWallet, transferAmount, receiverWallet);
        assertThat(senderWallet.getBalance()).isEqualTo(0.00);
        assertThat(receiverWallet.getBalance()).isEqualTo(200.00);

    }


}


