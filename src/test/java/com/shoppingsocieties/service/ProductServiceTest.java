package com.shoppingsocieties.service;

import com.shoppingsocieties.domain.Product;
import com.shoppingsocieties.domain.Sale;
import com.shoppingsocieties.domain.User;
import com.shoppingsocieties.domain.Wallet;
import com.shoppingsocieties.error.BadRequestException;
import com.shoppingsocieties.repository.ProductRepository;
import com.shoppingsocieties.repository.SaleRepository;
import com.shoppingsocieties.repository.WalletRepository;
import com.shoppingsocieties.testmodelbuilder.TestProductBuilder;
import com.shoppingsocieties.testmodelbuilder.TestSaleBuilder;
import com.shoppingsocieties.testmodelbuilder.TestUserBuilder;
import com.shoppingsocieties.testmodelbuilder.TestWalletBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Product Service Unit Test
 */

@RunWith(SpringRunner.class)
public class ProductServiceTest {

    @Autowired
    private ProductService productService;

    @MockBean
    private WalletService walletService;

    @MockBean
    private SaleService saleService;

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private WalletRepository walletRepository;

    @MockBean
    private SaleRepository saleRepository;

    @TestConfiguration
    static class ProductServiceTestConfig {
        @Bean
        public ProductService productService() {
            return new ProductService();
        }
    }

    @Test
    public void productPurchaseTest() {
        User testReceiver = new TestUserBuilder().id(1L).build();
        User testSender = new TestUserBuilder().id(2L).build();
        Product purchasedProduct = new TestProductBuilder().id(1L).build();
        Sale sale = new TestSaleBuilder().itemsLeft(1).build();

        Wallet testSenderWallet = new TestWalletBuilder().id(1L).build();
        Wallet testReceiverWallet = new TestWalletBuilder().id(2L).build();

        Mockito.when(walletRepository.findByUserId(testSender.getId())).thenReturn(Optional.of(testReceiverWallet));
        Mockito.when(walletRepository.findOneById(testReceiver.getId())).thenReturn(Optional.of(testSenderWallet));
        Mockito.when(productRepository.findOneById(purchasedProduct.getId())).thenReturn(Optional.of(purchasedProduct));
        Mockito.when(saleRepository.findOneByProductId(purchasedProduct.getId())).thenReturn(Optional.of(sale));

        assertThat(productService.productPurchase(1L, 2L)).isEqualTo(purchasedProduct);

    }

    @Test(expected = BadRequestException.class)
    public void testProductPurchaseSoldOutException() {
        User testReceiver = new TestUserBuilder().id(1L).build();
        User testSender = new TestUserBuilder().id(2L).build();
        Product purchasedProduct = new TestProductBuilder().id(1L).build();
        Sale sale = new TestSaleBuilder().itemsLeft(0).build();

        Wallet testSenderWallet = new TestWalletBuilder().id(1L).build();
        Wallet testReceiverWallet = new TestWalletBuilder().id(2L).build();

        Mockito.when(walletRepository.findByUserId(testSender.getId())).thenReturn(Optional.of(testReceiverWallet));
        Mockito.when(walletRepository.findOneById(testReceiver.getId())).thenReturn(Optional.of(testSenderWallet));
        Mockito.when(productRepository.findOneById(purchasedProduct.getId())).thenReturn(Optional.of(purchasedProduct));
        Mockito.when(saleRepository.findOneByProductId(purchasedProduct.getId())).thenReturn(Optional.of(sale));

        assertThat(productService.productPurchase(1L, 2L)).isEqualTo(purchasedProduct);
    }

}
