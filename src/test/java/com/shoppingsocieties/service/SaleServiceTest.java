package com.shoppingsocieties.service;

import com.shoppingsocieties.domain.Sale;
import com.shoppingsocieties.error.BadRequestException;
import com.shoppingsocieties.repository.SaleRepository;
import com.shoppingsocieties.testmodelbuilder.TestSaleBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Sale Service Unit Test
 */
@RunWith(SpringRunner.class)
public class SaleServiceTest {

    @Autowired
    private SaleService saleService;

    @MockBean
    private SaleRepository saleRepository;

    @TestConfiguration
    static class SaleServiceTestConfig {
        @Bean
        public SaleService saleService() {
            return new SaleService();
        }
    }

    @Test
    public void findAllByCountryTest() {
        String correctValue = "SG";
        String correctableValue = "sg";

        Sale testSale = new TestSaleBuilder().id(1L).build();
        Sale testSale1 = new TestSaleBuilder().id(2L).build();
        List<Sale> testSaleList = new ArrayList<>();
        testSaleList.add(testSale);
        testSaleList.add(testSale1);

        Mockito.when(saleRepository.findAllByCountry(correctValue)).thenReturn(testSaleList);
        Mockito.when(saleRepository.findAllByCountry(correctableValue)).thenReturn(testSaleList);

        assertThat(saleService.findAllByCountry(correctableValue)).isEqualTo(testSaleList);
        assertThat(saleService.findAllByCountry(correctValue)).isEqualTo(testSaleList);
    }

    @Test(expected = BadRequestException.class)
    public void findAllByCountryTestException() {
        String invalidValue = "sggggg";
        String nonAlphabetValue = "1";

        Sale testSale = new TestSaleBuilder().id(1L).build();
        Sale testSale1 = new TestSaleBuilder().id(2L).build();
        List<Sale> testSaleList = new ArrayList<Sale>();
        testSaleList.add(testSale);
        testSaleList.add(testSale1);

        Mockito.when(saleRepository.findAllByCountry(invalidValue)).thenReturn(testSaleList);
        Mockito.when(saleRepository.findAllByCountry(nonAlphabetValue)).thenReturn(testSaleList);

        assertThat(saleService.findAllByCountry(invalidValue)).isEqualTo(testSaleList);
        assertThat(saleService.findAllByCountry(nonAlphabetValue)).isEqualTo(testSaleList);

    }

}