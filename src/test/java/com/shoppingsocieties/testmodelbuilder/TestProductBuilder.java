package com.shoppingsocieties.testmodelbuilder;

import com.shoppingsocieties.domain.Product;
import org.springframework.beans.BeanUtils;

/**
 * A builder class for Product Domain Testing
 */

public class TestProductBuilder {

    private Long id;
    private Double price;

    public Product build() {
        Product product = new Product();
        BeanUtils.copyProperties(this, product);
        return product;
    }

    public TestProductBuilder id(Long id) {
        this.id = id;
        this.price = 100.00;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Double getPrice() {
        return price;
    }
}
