package com.shoppingsocieties.testmodelbuilder;

import com.shoppingsocieties.domain.Sale;
import org.springframework.beans.BeanUtils;

/**
 * A builder class for Sale Domain Testing
 */
public class TestSaleBuilder {
    private Long id;
    private Integer itemsLeft;

    public Sale build() {
        Sale sale = new Sale();
        BeanUtils.copyProperties(this, sale);
        return sale;
    }

    public TestSaleBuilder id(Long id) {
        this.id=id;
        return this;
    }
    public TestSaleBuilder itemsLeft(Integer itemsLeft) {
        this.itemsLeft=itemsLeft;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Integer getItemsLeft() {
        return itemsLeft;
    }
}
