package com.shoppingsocieties.testmodelbuilder;

import com.shoppingsocieties.domain.Currency;
import com.shoppingsocieties.domain.User;
import com.shoppingsocieties.domain.Wallet;
import org.springframework.beans.BeanUtils;

/**
 * A builder class for Wallet Domain Testing
 */

public class TestWalletBuilder {

    private Long id;

    private Double balance;

    private Currency currency;

    private User user;

    public Wallet build() {
        Wallet wallet = new Wallet();
        BeanUtils.copyProperties(this, wallet);
        return wallet;
    }

    public TestWalletBuilder id(Long id) {
        this.id = id;
        this.balance = 100.00;
        this.currency = Currency.SGD;
        this.user = new User();
        user.setId(1L);
        return this;
    }

    public TestWalletBuilder user(User user) {
        this.user = user;
        return this;
    }

    public TestWalletBuilder balance(Double balance) {
        this.balance = balance;
        return this;
    }


    public Long getId() {
        return id;
    }

    public Double getBalance() {
        return balance;
    }

    public User getUser() {
        return user;
    }
}
