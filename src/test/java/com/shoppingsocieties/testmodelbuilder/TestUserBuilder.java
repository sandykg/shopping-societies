package com.shoppingsocieties.testmodelbuilder;

import com.shoppingsocieties.domain.User;
import org.springframework.beans.BeanUtils;

/**
 * A builder class for User Domain Testing
 */

public class TestUserBuilder {

    private Long id;

    public User build() {
        User user = new User();
        BeanUtils.copyProperties(this, user);
        return user;
    }

    public TestUserBuilder id(Long id) {
        this.id =id;
        return this;
    }

    public Long getId() {
        return id;
    }
}
