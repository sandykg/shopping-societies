# ShoppingSocieties
## Introduction

This is an implementation of challenge given by Funding Societies.


### Building and Running

The code uses Gradle for build automation. The project provides Gradle wrapper, hence a separate
installation of Gradle might not be required. If you prefer using you local gradle version please use 'gradle' instead
of './gradlew' in the below commands.

Below are the gradle tasks which can be used to test this solution.

**Running & Testing the program:**

The project uses [Swagger](https://swagger.io). After running the project using the below task run [http://localhost:8080/swagger-ui.html#/](http://localhost:8080/swagger-ui.html#/) on your favourite browser to test the API

	./gradlew bootRun

**Creating a build:**

If you prefer to generate .war and run, use this command. The generated .war file will be found in *build/libs/shoppingsocieties-0.0.1-SNAPSHOT.war*
 
	./gradlew build


**To run JUnit test cases:**

	./gradlew test


**Running Load test:**

This solution uses [Gatling](https://gatling.io) to do performance testing. The results can be found under *build/gatling-results*

	./gradlew loadTest

### Implementation

The program requires minimum of Java 1.8 and build using [Spring boot](https://spring.io/projects/spring-boot).

### Assumptions

* Security features like authentication, HTTP response headers, session have not been implemented.
* i18n is not implemented
* For the sake of simplicity, H2 in memory database has been used. This may or may not be suitable for production.
* When flash sale has many concurrent customers accessing same products in inventory, this project assumes that lock is done at cart level as API level lock might affect performance.

### Error Handling
* Some web services return status codes that reflects success (status codes from 200 to 206 and from 300 to 307) but include a message body that describes an error condition. Doing this prevents HTTP-aware software from detecting errors. For example, a cache will store it as successful response and serve it to subsequent clients even when clients may be able to make a successful request.
* This project attempts to cover and throw meaningful HTTP error codes with custom error body.

### Future enhancements/Todo's:
* The project uses "Double" for storing all monetary values as the problem statement is quite simple. But for a real time API with more functionality, [Martin Fowler's Money pattern](https://martinfowler.com/eaaCatalog/money.html) should be implemented. Or packages like [Joda Money](http://www.joda.org/joda-money/) can be used.
* For more scalable solution, a stream-processing packages like [Kafka](https://kafka.apache.org) may be used.
